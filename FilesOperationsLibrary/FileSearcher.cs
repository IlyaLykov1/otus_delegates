﻿using System;
using System.Collections.Generic;
using System.IO;

namespace FilesOperationsLibrary
{
    public class FileSearcher
    {
        private HashSet<string> _filesToFind;
        private string _directoryName;

        public FileSearcher(string directoryName, IList<string> filesToFind)
        {
            _filesToFind = new HashSet<string>(filesToFind);
            _directoryName = directoryName;
        }

        public void TraverseFileCatalog()
        {
            Stack<string> directoriesStack = new Stack<string>();
            directoriesStack.Push(_directoryName);

            while (directoriesStack.Count != 0)
            {
                string name = directoriesStack.Pop();

                DirectoryInfo directoryInfo = new DirectoryInfo(name);
                foreach (var fileInfo in directoryInfo.GetFiles())
                {
                    if (_filesToFind.Contains(fileInfo.Name))
                    {
                        OnFileFound(fileInfo.Name);
                    }
                }

                foreach (var dir in directoryInfo.GetDirectories())
                {
                    directoriesStack.Push(dir.FullName);
                }
            }
        }

        protected virtual void OnFileFound(string fileName)
        {
            FileFound?.Invoke(this, new FileFoundEventArgs(fileName));
        }

        public event EventHandler<FileFoundEventArgs> FileFound;
    }

    public class FileFoundEventArgs : EventArgs
    {
        public string FileName { get; }
        public FileFoundEventArgs(string fileName)
        {
            FileName = fileName;
        }
    }
}
