﻿using System;
using System.Collections;

namespace GenericExtensionsMethods
{
    public static class GenericExtensions
    {
        public static T GetMax<T>(this IEnumerable e, Func<T, float> getParameter) where T : class
        {
            T maxElement = null;
            float max = float.MinValue;
            foreach (T item in e)
            {
                float curr = getParameter(item);
                if(curr > max)
                {
                    maxElement = item;
                }
            }
            return maxElement;
        }
    }
}
