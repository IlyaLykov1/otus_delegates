﻿using FilesOperationsLibrary;
using GenericExtensionsMethods;
using System;
using System.Collections.Generic;

namespace ConsoleUI
{
    class Program
    {
        static void Main(string[] args)
        {
            FileSearch(args);
            MaxFind();
        }

        private static void FileSearch(string[] args)
        {
            Console.WriteLine("Enter directory name:");
            string directoryName = Console.ReadLine();

            IList<string> filesToFind = new List<string>();
            if (args.Length == 0)
            {
                while (true)
                {
                    Console.WriteLine("Enter file name to find: (press \"no\" to exit)");
                    string input = Console.ReadLine();
                    if (input == "no") break;
                    filesToFind.Add(input);
                }
            }

            FileSearcher fileSearcher = new FileSearcher(directoryName, filesToFind);
            fileSearcher.FileFound += FileSearcher_FileFound;
            fileSearcher.TraverseFileCatalog();
            fileSearcher.FileFound -= FileSearcher_FileFound;

            Console.WriteLine("Traverse is finished");
        }

        private static void FileSearcher_FileFound(object sender, FileFoundEventArgs e)
        {
            Console.WriteLine($"{e.FileName} is found");
        }

        private static void MaxFind()
        {
            Func<string, float> funcString = GetParameter;
            List<string> words = new List<string> { "a", "ab", "abc" };
            Console.WriteLine($"Max from words list is '{words.GetMax(funcString)}'");

            Func<House, float> funcHouse = GetParameter;
            List<House> houses = new List<House> { new House { TotalArea = 100 }, new House { TotalArea = 200 } };
            Console.WriteLine($"Max from house list is '{houses.GetMax(funcHouse)}'");
        }

        public static float GetParameter<T>(T param) where T : class => param switch
        {
            string s => s.Length,
            House house => house.TotalArea,
            _ => 0,
        };
    }
}
