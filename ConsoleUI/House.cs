﻿namespace ConsoleUI
{
    public class House
    {
        public string Address { get; set; }
        public float TotalArea { get; set; }

        public override string ToString()
        {
            return $"House with {TotalArea} square meters.";
        }
    }
}
